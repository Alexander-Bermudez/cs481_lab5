import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab 5 - Presentation',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Lab 5 - Presentation'),
          backgroundColor: Color(0xff5808e5),
          actions: <Widget>[
            PopupMenuButton(
              itemBuilder: (context) => [
                PopupMenuItem(
                  child: Text('Add Content'),
                ),
                PopupMenuItem(
                  child: Text('News'),
                ),
                PopupMenuItem(
                  child: Text('Settings'),
                ),
              ],
              tooltip: "Press for additional options",
            ),
          ],
        ),
        body: Tabs(),
      ),
    );
  }
}

class Tabs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Fixed Tabs'),
          automaticallyImplyLeading: false,
          backgroundColor: Color(0xff5808e5),
          bottom: TabBar(
            indicatorColor: Colors.white,
            tabs: [
              Tab(text: 'SnackBar', icon: Icon(Icons.favorite)),
              Tab(text: 'ToolTips ', icon: Icon(Icons.music_note)),
              Tab(text: 'Tabs', icon: Icon(Icons.search)),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            SnackBarPage(),
            Center(
              child: FloatingActionButton(
                child: Icon(Icons.add),
                backgroundColor: Color(0xff5808e5),
                onPressed: () {},
                tooltip: "Add Additional Content",
              ),
            ),
            Center(child: Text('Tabs')),
          ],
        ),
      ),
    );
  }
}

class SnackBarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: RaisedButton(
        onPressed: () {
          final showSnackBar = SnackBar(
            content: Text('Hey! This is a SnackBar Notification!'),
            action: SnackBarAction(
              label: 'Go Away',
              onPressed: () {
                //Perform action here
              },
            ),
            duration: const Duration(seconds: 10),
            backgroundColor: Colors.black45,
            behavior: SnackBarBehavior.floating,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(30))),
          );

          // Find the Scaffold in the widget tree and use
          // it to show a SnackBar.
          Scaffold.of(context).showSnackBar(showSnackBar);
        },
        child: Text("DON'T PRESS THIS BUTTON"),
      ),
    );
  }
}
